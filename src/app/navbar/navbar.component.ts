import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  get nomeUsuario() {
    return localStorage.getItem('username');
  }

  temPermissao(permissao: string){
    return permissao == localStorage.getItem('permissao');
  }

}

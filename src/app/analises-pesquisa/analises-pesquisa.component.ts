import { Component, OnInit, ViewChild } from '@angular/core';
import { Table } from 'primeng/components/table/table';
import { LazyLoadEvent, ConfirmationService } from 'primeng/components/common/api';
import { AnalisesService } from './../analises.service';
import { ToastaService } from 'ngx-toasta';

@Component({
  selector: 'app-analises-pesquisa',
  templateUrl: './analises-pesquisa.component.html',
  styleUrls: ['./analises-pesquisa.component.css']
})
export class AnalisesPesquisaComponent implements OnInit {

  status = [
    { label: 'PENDENTE', value: 1 },
    { label: 'APROVADO', value: 2 },
    { label: 'NEGADO', value: 3 },
  ];

  analises = [];
  @ViewChild('tabela', {static: true}) grid: Table;

  constructor(
    private analisesService: AnalisesService,
    private toastaService:ToastaService,
    private confirmation: ConfirmationService
   ) { }

  ngOnInit() {
    this.pesquisar();
  }

  pesquisar(pagina = 0) {
    this.analisesService.pesquisar()
    .then(analises => this.analises = analises);
  }

  efetuarAnalise(id: number, status: number) {
    this.confirmation.confirm({
      message: 'Tem certeza que deseja '+(status == 1 ? 'APROVAR': 'REPROVAR')+ ' esta análise?',
      accept: () => {
        this.atualizarStatus(id, status);
      }
    });
  }

  atualizarStatus(id: number, status: number) {
    this.analisesService.atualizarStatus(id,status)
      .then(() => {
        this.grid.reset();
        this.toastaService.success('Análise atualizada com sucesso!');
      });
  }

  aoMudarPagina(event: LazyLoadEvent) {
    const pagina = event.first / event.rows;
    this.pesquisar(pagina);
  }

  temPermissao(permissao: string){
    return permissao == localStorage.getItem('permissao');
  }
}

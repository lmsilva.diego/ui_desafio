import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class AuthService {

  authUrl = 'http://localhost:8080/usuarios';

  constructor(private http: HttpClient,private router: Router) { }

  login(usuario: string, senha: string): Promise<any> {
    return this.http.get(`${this.authUrl}/${usuario}`)
    .toPromise()
    .then(response => this.armazenarJson(response));
    
  }

  private armazenarJson(response: object){
    if(response != null){
      localStorage.setItem('username', response['username']);
      localStorage.setItem('password', response['password']);
      localStorage.setItem('permissao', response['permissao']);
      this.router.navigate(['/analises']);
    }
    
  }

}
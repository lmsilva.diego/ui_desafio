import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { InputTextModule } from 'primeng/components/inputtext/inputtext';
import { ButtonModule } from 'primeng/components/button/button';
import { TableModule } from 'primeng/components/table/table';
import { TooltipModule } from 'primeng/components/tooltip/tooltip';
import { AnalisesPesquisaComponent } from './analises-pesquisa/analises-pesquisa.component';
import { NavbarComponent } from './navbar/navbar.component';
import { PortadoresPesquisaComponent } from './portadores-pesquisa/portadores-pesquisa.component';
import { PortadoresCadastroComponent } from './portadores-cadastro/portadores-cadastro.component';

import { CalendarModule } from 'primeng/components/calendar/calendar';
import { DropdownModule } from 'primeng/components/dropdown/dropdown';
import { InputMaskModule } from 'primeng/components/inputmask/inputmask';
import { HttpClientModule } from '@angular/common/http';
import { PortadoresService } from './portadores.service';
import { AnalisesService } from './analises.service';
import {ToastaModule} from 'ngx-toasta';
import { ConfirmDialogModule } from 'primeng/components/confirmdialog/confirmdialog';
import { ConfirmationService } from 'primeng/components/common/api';
import { FormsModule }   from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { SegurancaModule } from './seguranca/seguranca.module';
import { AuthService } from './seguranca/auth.service';

const routes: Routes = [
  { path: 'portadores', component: PortadoresPesquisaComponent },
  { path: 'portadores/novo', component: PortadoresCadastroComponent },
  { path: 'portadores/:id', component: PortadoresCadastroComponent },
  { path: 'analises', component: AnalisesPesquisaComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    AnalisesPesquisaComponent,
    NavbarComponent,
    PortadoresPesquisaComponent,
    PortadoresCadastroComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    InputTextModule,
    ButtonModule,
    TableModule,
    TooltipModule,
    CalendarModule,
    DropdownModule,
    InputMaskModule,
    HttpClientModule,
    ToastaModule.forRoot(),
    ConfirmDialogModule,
    FormsModule,
    RouterModule,
    RouterModule.forRoot(routes),
    SegurancaModule
  ],
  providers: [PortadoresService, ConfirmationService, AnalisesService, AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Pessoa } from './core/model';

@Injectable({
  providedIn: 'root'
})
export class PortadoresService {

  portadoresUrl = 'http://localhost:8080/pessoas';

  constructor(private http: HttpClient) { }

  pesquisar(): Promise<any> {
    return this.http.get(`${this.portadoresUrl}`)
    .toPromise()
    .then(response => response);
  }

  excluir(id: number): Promise<void> {
    return this.http.delete(`${this.portadoresUrl}/${id}`)
      .toPromise()
      .then(() => null);
  }  

  atualizar(pessoa: Pessoa): Promise<Pessoa> {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');

    return this.http.put(`${this.portadoresUrl}/${pessoa.id}`,
      JSON.stringify(pessoa), { headers })
      .toPromise()
      .then(response => {
        const portadorAlterado = response as Pessoa;
        return portadorAlterado;
      });
  }

  buscarPorCodigo(id: number): Promise<Pessoa> {

    return this.http.get(`${this.portadoresUrl}/${id}`)
      .toPromise()
      .then(response => {
        const portador = response as Pessoa;
        return portador;
      });
  }

}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';

import { Analise } from './core/model';

@Injectable({
  providedIn: 'root'
})
export class AnalisesService {

  analisesUrl = 'http://localhost:8080/analises';

  constructor(private http: HttpClient) { }

  pesquisar(): Promise<any> {
    return this.http.get(`${this.analisesUrl}`)
    .toPromise()
    .then(response => response);
  }

  excluir(id: number): Promise<void> {
    return this.http.delete(`${this.analisesUrl}/${id}`)
      .toPromise()
      .then(() => null);
  }

  adicionar(analise: Analise): Promise<Analise> {

    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');

    return this.http.post<Analise>(
      this.analisesUrl, analise, {headers})
      .toPromise();
  }

  atualizarStatus(id: number, status: number): Promise<Analise> {

    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');

    return this.http.put<Analise>(
      `${this.analisesUrl}/${id}`, status, {headers})
      .toPromise();
  }


}

export class Pessoa {
    id: number;
    nome: string;
    cpf: string;
  }

  export class Analise {
    id: number;
    status = 0;
    dataAnalise: Date;
    dataSolicitacao: number = Date.now();;
    pessoa = new Pessoa();
  }

  export class Usuario {
    id: number;
    username: string;
    password: string;
    permissao: number;
  }

import { Component, OnInit, ViewChild } from '@angular/core';
import { PortadoresService } from './../portadores.service';
import { Table } from 'primeng/components/table/table';
import { LazyLoadEvent, ConfirmationService } from 'primeng/components/common/api';
import { ToastaService } from 'ngx-toasta';


@Component({
  selector: 'app-portadores-pesquisa',
  templateUrl: './portadores-pesquisa.component.html',
  styleUrls: ['./portadores-pesquisa.component.css']
})
export class PortadoresPesquisaComponent implements OnInit {

  pessoas = [];
  @ViewChild('tabela', {static: true}) grid: Table;

  constructor(
              private portadoresService: PortadoresService,
              private toastaService:ToastaService,
              private confirmation: ConfirmationService
             ) { }

  ngOnInit() {
    this.pesquisar();
  }

  pesquisar(pagina = 0) {
    this.portadoresService.pesquisar()
      .then(pessoas => this.pessoas = pessoas);
  }

  aoMudarPagina(event: LazyLoadEvent) {
    const pagina = event.first / event.rows;
    this.pesquisar(pagina);
  }

  confirmarExclusao(pessoa: any) {
    this.confirmation.confirm({
      message: 'Tem certeza que deseja excluir?',
      accept: () => {
        this.excluir(pessoa);
      }
    });
  }

  excluir(pessoa: any) {
    this.portadoresService.excluir(pessoa.id)
      .then(() => {
        this.grid.reset();
        this.toastaService.success('Portador excluído com sucesso!');
      });
  }
}
import { Component, OnInit } from '@angular/core';
import { Analise } from './../core/model';
import { AnalisesService } from './../analises.service';
import { FormControl } from '@angular/forms';
import { ToastaService } from 'ngx-toasta';
import { ActivatedRoute } from '@angular/router';
import { PortadoresService } from './../portadores.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-portadores-cadastro',
  templateUrl: './portadores-cadastro.component.html',
  styleUrls: ['./portadores-cadastro.component.css']
})
export class PortadoresCadastroComponent implements OnInit {

  analise = new Analise();

  constructor(
    private analiseService: AnalisesService,
    private toastaService:ToastaService,
    private route: ActivatedRoute,
    private portadoresService: PortadoresService,
    private router: Router
  ) { }

  ngOnInit() {
    const id = this.route.snapshot.params['id'];

    if (id) {
      this.carregarPortador(id);
    }
  }

  get editando() {
    return Boolean(this.analise.id)
  }

  carregarPortador(id: number) {
    this.portadoresService.buscarPorCodigo(id)
      .then(pessoa => {
        this.analise.pessoa = pessoa;
      })
  }

  salvar(form: FormControl) {
    if (this.editando) {
      this.atualizarProposta(form);
    } else {
      this.adicionarProposta(form);
    }
  }

  adicionarProposta(form: FormControl) {
    this.analiseService.adicionar(this.analise)
      .then(() => {
        this.toastaService.success('Proposta cadastrada. Favor aguardar análise!');

        form.reset();
        this.analise = new Analise();
      })
  }

  atualizarProposta(form: FormControl) {
    this.portadoresService.atualizar(this.analise.pessoa)
      .then(pessoa => {
        this.analise.pessoa = pessoa;

        this.toastaService.success('Proposta atualizada. Favor aguardar análise!');
      })
  }

}
